// 将所有的登陆相关的接口都放这里，登陆模块的所有接口都放这
// export 与 import的使用
import axios from 'axios'
import { Toast } from 'vant'
import { getToken, removeToken } from '@/utils/locale.js'
import router from '@/router/index.js' // router===this.$router
// 个性定制化一个axios,产生一个定制后axios,不改变原axios
const _axios = axios.create({
  baseURL: 'http://interview-api-t.itheima.net/h5'
})

// 请求拦截与响应拦截
// 请求拦截:统一token处理
_axios.interceptors.request.use(
  config => {
    if (getToken()) {
      config.headers.Authorization = `Bearer ${getToken()}`
    }
    // console.log('请求拦截', config)
    // config.headers.xxx = '91真牛'
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// 响应拦截：统一错误处理
_axios.interceptors.response.use(
  res => {
    // console.log('响应拦截的res', res)
    return res.data
  },
  error => {
    // console.log('接口请求出错了')
    // 统一的接口请求出错处理
    // 拦截错误，提示错误信息
    // console.log('error')
    // console.dir(error)
    // 有错误信息才提示
    // 并非所有的错误都有response跑出来
    // 有response和message才提示
    // console.log('error', error)
    if (error.response && error.response.data.message) {
      // error.response.data.message是后端 给我们的，我们要考虑可能后端不提供的场景
      Toast.fail(error.response.data.message)
      // 如果是token失效，清除token跳转到登陆页
      if (error.response.data.code === 401) {
        // 清除token
        removeToken()
        // 跳转到登陆页
        // this.$router===路由的实例对象
        router.push('/login')
      }
    }
    return Promise.reject(error)
  }
)

export default _axios
