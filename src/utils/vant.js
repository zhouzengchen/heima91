import {
  Button,
  Tabbar,
  TabbarItem,
  NavBar,
  Field,
  Form,
  Toast,
  Cell,
  CellGroup,
  List,
  Icon,
  Grid,
  GridItem
} from 'vant'
import Vue from 'vue'
Vue.use(Button)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(NavBar)
Vue.use(Field)
Vue.use(Form)
Vue.use(Toast)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(List)
Vue.use(Icon)
Vue.use(Grid)
Vue.use(GridItem)
