import Vue from 'vue'
import App from './App.vue'

import router from './router'
// 使用vant
// 全局使用：不要的点：文件会较大一些，优点：使用方便
// 1:下载  npm i vant@latest-v2
// 2：复制下面代码
//  按需使用：我页面使用了哪个组件，你就导入哪个组件
// 1：下载插件  npm i babel-plugin-import -D  有版本问题都加  --force
// 2:配制babel 复制官网配制就可以了
// 3:按需要导入  import { Button } from 'vant'   Vue.use(Button)
// 后面要是vant组件用的多了，
// 封装抽离下面的代码
// 封装一个js导入到main.js就可以了

// 适配
// 1:rem   2:vw
// 早期 ：rem的天下  现在新项目基本都是vw
// rem  1:html下的font-size设置成屏幕宽度的1/10  amfe-flexible.js  2:使用插件转换px成rem  postcss-px-to-rem
// vw:相对于视口的宽度.
import '@/utils/vant'
import ArticleItem from '@/components/article-item.vue'
Vue.component(ArticleItem.name, ArticleItem)

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
