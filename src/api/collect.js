import request from '@/utils/request.js'
// 获取收藏列表
export const interviewOptList = params => {
  return request({
    url: '/interview/opt/list',
    params
  })
}
