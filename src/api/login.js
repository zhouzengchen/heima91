import request from '@/utils/request'

export const toLogin = data => {
  return request({
    url: '/user/login',
    method: 'post',
    data: data
  })
}
