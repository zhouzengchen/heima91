import request from '@/utils/request'
// 获取文章详情

export const interviewShow = id => {
  return request({
    url: '/interview/show',
    params: {
      id: id
    }
  })
}

// 点赞与收藏
export const interviewOpt = data => {
  return request({
    url: '/interview/opt',
    method: 'post',
    data
  })
}
