// login很多公司把用户相关的api都放user.js
import request from '@/utils/request.js'
// 注册的api
export const userRegister = data => {
  return request({
    url: '/user/register',
    method: 'post',
    data: data
  })
}
// 登录接口
export const userLogin = data => {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}
// 获取用户信息
export const userCurrentUser = () => {
  return request({
    url: '/user/currentUser'
  })
}
