import request from '@/utils/request.js'
// import { getToken } from '@/utils/locale.js'
// 获取面经列表
export const interviewQuery = params => {
  return request({
    url: '/interview/query',
    params
    // headers: {
    //   Authorization: `Bearer ${getToken()}`
    // }
  })
}
