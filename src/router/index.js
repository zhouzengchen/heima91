import Vue from 'vue'
import VueRouter from 'vue-router'
import { getToken } from '@/utils/locale'
// import Login from '@/views/login.vue'
// const Login = () => import('@/views/login.vue')
// const Register = () => import('@/views/register.vue')
// const Detail = () => import('@/views/detail.vue')
// const Layout = () => import('@/views/layout.vue')
// const Article = () => import('@/views/article.vue')
// const Collect = () => import('@/views/collect.vue')
// const Like = () => import('@/views/like.vue')
// const My = () => import('@/views/my.vue')
// const PlayGround = () => import('@/views/playGround.vue')
// 路由懒加载
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: () => import('@/views/login.vue')
    // component 判断如果是方法，它会调用的
  },
  {
    path: '/playground',
    component: () => import('@/views/playGround.vue')
  },
  {
    path: '/register',
    component: () => import('@/views/register.vue')
  },
  {
    path: '/detail',
    component: () => import('@/views/detail.vue')
  },
  {
    path: '/',
    redirect: '/article',
    component: () => import('@/views/layout.vue'),
    children: [
      {
        path: '/article',
        component: () => import('@/views/article.vue')
      },
      {
        path: '/collect',
        component: () => import('@/views/collect.vue')
      },
      {
        path: '/like',
        component: () => import('@/views/like.vue')
      },
      {
        path: '/my',
        component: () => import('@/views/my.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 白名单页面
const whitePage = ['/login', '/register']
// 前置守卫：还没有到想去的路由，任何路由跳转前都要经过它
router.beforeEach((to, from, next) => {
  // console.log('to', to)
  // console.log('from', from)
  // to:想去的页面的路由基本信息、
  // from:从哪来的页面的路由基本信息
  // next:是否允许通过  next()正常通过
  // next类似于this.$router.push  比如:next('/register')
  // 默认：next()
  // 需求：如果我的页面，我要去的页面是点赞页面，产生一个随机数，大于0.5就通过，不然就去收藏页面
  // if (from.path === '/my') {
  //   if (to.path === '/like') {
  //     const num = Math.random()
  //     console.log(num)
  //     if (num > 0.5) {
  //       next()
  //     } else {
  //       next('/collect')
  //     }
  //   } else {
  //     next()
  //   }
  // } else {
  //   next()
  // }

  /*
     有token
        正常进入
      没有token
         难道所有页面都不能去吗？
             可以去:登录  注册  404 等页面应该是可以的
             其它的不可以，跳转到登录界面  */
  if (getToken()) {
    next()
  } else {
    // 有可能to.path是大写,需要转换成小写再比较
    if (whitePage.includes(to.path.toLowerCase())) {
      next()
    } else {
      next('/login')
    }
  }

  // 注意点：next(path地址时)它会再经过一次beforeEach,next()不会再经过beforeEach
})

// 后置守卫：已到达想去的路由
router.afterEach((to, from) => {
  // to:想去的页面的路由基本信息、
  // from:人哪来的页面的路由基本信息
  // console.log('我已到了我想去的页面')
})
// 获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
// 修改原型对象中的push方法
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}
export default router
